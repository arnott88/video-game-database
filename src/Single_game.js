import React, { useState, useEffect } from 'react'
import Axios from'axios'


const SingleGame = (props) => {
    const { guid } = props;

    const [gameData, setGameData] = useState(null)
    const [loader, setLoader] = useState(true)

    const ApiCallSingle = async (search) => {

        let _guid = guid

        try {
            const response= await Axios.get(`http://www.giantbomb.com/api/game/${guid}/?api_key=c094f7be39a62cad2dc8db2f5a59ffe15936a033&format=JSON`)
            setGameData({...response.data})
            console.log('game data', response.data)
            }   
            catch(error) 
        {
            console.log(error)
        }
    }

useEffect(() => {
    ApiCallSingle()
},[])

useEffect (() => {
    if(gameData !== null) {
        setLoader(false)
    }
},[gameData])
    
return( 
    <div>

    {!loader
     ? <div>
        <p>{gameData.results.name}</p>
        <img src={gameData.results.image.super_url}/>
        <p>{gameData.results.deck}</p>
      </div>
     :  <p>loading...</p>}
            
    </div>

    )
}

export default SingleGame