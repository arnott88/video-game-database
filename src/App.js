import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Axios from 'axios'
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import FrontPage from './Front_page'
import SingleGame from './Single_game'




function App() {

  const [guid, setGuid] = useState('')
  
  const GuidStateChanger = (b) => {
    setGuid(b)
  }

  return (

    <div className="App">
      
      <Router>
        <Route exact path="/"render={(props) => {
          return <FrontPage {...props} GuidStateChanger={GuidStateChanger}/>}}/>
        <Route exact path="/SingleGame"render={(props) => {
          return <SingleGame {...props} guid={guid}/>}}/>
      
    </Router>

    </div>
  );
}

export default App;


//Giant bomb API key: c094f7be39a62cad2dc8db2f5a59ffe15936a033