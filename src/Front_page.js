import React, { useState, useEffect } from 'react'
import Axios from'axios'


const FrontPage = (props) => {
    const { GuidStateChanger } = props;

    const [loader, setLoader] = useState(true)
    const [gameDB, setGameDB] = useState(null)
    const [search, setSearch] = useState('')
    

    const ApiCall = async (search) => {

        let title = search

        try {
            const response= await Axios.get(`https://www.giantbomb.com/api/search?api_key=c094f7be39a62cad2dc8db2f5a59ffe15936a033&format=JSON&query=${title}&resources=game`)
            setGameDB({...response.data})
            console.log('game data', response.data)
            }   
            catch(error) 
        {
            console.log(error)
        }
    }

    const onChangeHandler = (e) => {
        setSearch(e.target.value)
    }

    const handleSubmit = (e) => {
        ApiCall(search)
    }

    const getGuid = (i) => {
        GuidStateChanger(gameDB.results[i].guid)
        // console.log(guid)
        props.history.push("/SingleGame")
    }

    useEffect(() => {
        if(gameDB !== null){
        setLoader(false)
        console.log(gameDB)
        }
    }, [gameDB])
    
return( 
    <div>

        <main>

        <div>
        <input placeholder=" search for game:" value={search} onChange={onChangeHandler}></input>
        </div>

        { loader
        ? <p>Type a game title and recieve results</p>
        : gameDB.results.map((result, i) => {
            return <div>
                    <p onClick={() => getGuid(i)}>{result.name}</p>
            </div>
            })
        }

        <p onClick={handleSubmit}>Search</p>

        </main>
            
    </div>

    )
}

export default FrontPage